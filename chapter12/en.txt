It is mainly based on the influence of the language and its degree of universality in the world to decide whether to establish the language as the working language of the United Nations;
First include the languages spoken by the five permanent members:
The common language of the United Kingdom and the United States is English, which is widely used by many member states of the Commonwealth and former colonial countries;
French French is widely used in many regions in Europe, Africa, America and Asia;
Russian Russian is widely used in member states of the former Soviet Union and Eastern Europe;
The Chinese language in China has the largest population in the world, and it is also very influential in Southeast Asia;
Spanish is because there were colonies everywhere when Spain was strong in the 15th and 16th centuries. Many countries in Europe and the Americas used Spanish as the official language. This is also the predecessor of the United Nations-the working language that has been used until now during the League of Nations period;
In 1973, due to the influence of Arabic in the Middle East and the entire Arab world, it was also established as the working language of the United Nations.
