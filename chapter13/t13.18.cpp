/*************************************************************************************
* Copyright (c) 2020 xffish
* c_plus_plus_primer_exercise is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
* http://license.coscl.org.cn/MulanPSL2
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*************************************************************************************/

//int序号生成器麻烦，我换了一个UUID生成器
//Special notes
//    clang/g++ users: both -std=c++11 and -lrt may be required when compiling sole.cpp

#include "sole.hpp"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

class Employee
{
friend void print(const Employee&);
public:
    explicit Employee(const string& nameParam):name(nameParam)
    {
        ++eidGlobal;
        eid = eidGlobal;
    }
    //不允许拷贝
    Employee(const Employee&)=delete;
    //不允许赋值
    Employee& operator=(const Employee&)=delete;
    //不需要自定义析构函数
private:
    string name;
    int eid;
    static int eidGlobal;
};

int Employee::eidGlobal = 0;

void print(const Employee& emp)
{
    cout << "name:" << emp.name << ",eid=" << emp.eid << endl;
}

int main()
{
    Employee e1("jim");
    Employee e2("tom");
    print(e1);
    print(e2);
    return 0;
}
