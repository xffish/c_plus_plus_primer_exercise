/*************************************************************************************
* Copyright (c) 2021 xffish
* c_plus_plus_primer_exercise is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
* http://license.coscl.org.cn/MulanPSL2
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*************************************************************************************/

没定义析构函数，编译器会自行定义一个合成析构函数，不会主动释放成员变量string* 指针指的空间，导致内存泄漏。
没定义拷贝构造函数,编译器会自行定义一个合成拷贝构造函数，会直接对指针赋值，使得两个 HasPtr 对象的string指针实际指的是同一块内存地址，在析构时会导致对同一块地址delete两次